window.addEventListener("DOMContentLoaded", async () => {
    console.log('DOM content fully loaded and parsed.');

    const url = "http://localhost:8000/api/locations/"

    try{
        const response = await fetch(url);
        console.log("Fetched URL");

        if (!response.ok) {
            // console.log("Response is not okay")
            throw new Error('Response is not ok');

        }else {
            const data = await response.json();
            console.log(data);

            const selectTag = document.querySelector("#location");

            for (let location of data.locations) {
                const option = document.createElement("option");
                option.value = location.id;
                option.innerHTML = location.name;
                selectTag.append(option);

            }
        }

    const formTag = document.getElementById('create-conference-form');
    formTag.addEventListener('submit', async event => {
        event.preventDefault();
        // console.log('need to submit the form data');

        const formData = new FormData(formTag);
        const json = JSON.stringify(Object.fromEntries(formData));

        const conferenceUrl = "http://localhost:8000/api/conferences/";
        const fetchConfig = {
            method: "post",
            body: json, 
            headers: {
                'Content-Type': 'application/json',
            }
        };
        // console.log(fetchConfig);

        const response = await fetch(conferenceUrl, fetchConfig);
        if (response.ok) {
            formTag.requestFullscreen();
            const newConference = await response.json();
            console.log(newConference);
        }
    });



    } catch(e) {
        // console.log("error", e);

        const alertPlaceholder = document.getElementById("alert")

            const wrapper = document.createElement('alert')
            wrapper.innerHTML = '<div class="alert alert-warning alert-dismissible fade show" role="alert">Invalid URL<button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button></div>'
            
            alertPlaceholder.append(wrapper);
  
    }

});