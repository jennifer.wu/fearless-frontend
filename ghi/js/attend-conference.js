window.addEventListener('DOMContentLoaded', async () => {
    const selectTag = document.getElementById('conference');
    
    const url = 'http://localhost:8000/api/conferences/';
    
    try{
        const response = await fetch(url);
        
        if (response.ok) {
            // console.log("Fetched Url")
            
            const data = await response.json();
            // console.log(data);
            
            for (let conference of data.conferences) {
                const option = document.createElement('option');
                option.value = conference.href;
                option.innerHTML = conference.name;
                selectTag.appendChild(option);
            }

            // Here, add the 'd-none' class to the loading icon
            const loadingIconTag = document.getElementById("loading-conference-spinner");
            loadingIconTag.classList.add("d-none");
            
            // Here, remove the 'd-none' class from the select tag
            selectTag.classList.remove('d-none');
        }


        const formTag = document.getElementById('create-attendee-form');
        formTag.addEventListener('submit', async event => {
            event.preventDefault();

            const formData = new FormData(formTag);
            const json = JSON.stringify(Object.fromEntries(formData));
            // console.log(formData);

            const attendConferenceUrl = "http://localhost:8001/api/attendees/";

            // console.log(attendConferenceUrl)

            const fetchConfig = {
                method: "post",
                body: json,
                headers: {
                    'Content-Type': 'application.json',
                }
            }
            // console.log(fetchConfig);

            const response = await fetch(attendConferenceUrl, fetchConfig);
            console.log(response);

            if (response.ok) {
                const successTag = document.getElementById("success-message");
                successTag.classList.add('d-none');

            }

        
        });
    
    } catch(e){
        console.log("error", e)
    }


  
  });