function createCard(name, description, pictureUrl, newStartDate, newEndDate) {
    return `
    <div class="col">
      <div class="card shadow p-3 mb-5 bg-white rounded">
        <img src="${pictureUrl}" class="card-img-top">
        <div class="card-body">
          <h5 class="card-title">${name}</h5>
          <p class="card-text">${description}</p>
        </div>
        <div class="card-footer">
        <small class="text-muted">${newStartDate} - ${newEndDate}</small>
      </div>
      </div>
    </div>
    `;
  }




window.addEventListener('DOMContentLoaded', async () => {
    
    // const url = 'http://localhost:8000/api/conferences/';
    // const response = await fetch(url);
    // console.log(response);
    
    // const data = await response.json();
    // console.log(data);



    // const url = 'http://localhost:8000/api/conferences/';

    // try {
    //   const response = await fetch(url);
  
    //   if (!response.ok) {
    //     // Figure out what to do when the response is bad
    //     // throw new Error("Response not ok");
    //   } else {
    //     const data = await response.json();

    //     // show conference name
    //     const conference = data.conferences[0];
    //     const nameTag = document.querySelector('.card-title');
    //     nameTag.innerHTML = conference.name;

    //     // fetch the conference details
    //     const detailUrl = `http://localhost:8000${conference.href}`;
    //     const detailResponse = await fetch(detailUrl);
    //     if (detailResponse.ok) {
    //         const details = await detailResponse.json();
    //     //   console.log(details);
    //       // fetch the conference description out of returned data
    //         const description = details.conference.description;
    //         const descriptionTag = document.querySelector('.card-text');
    //         descriptionTag.innerHTML = description;
    //         // console.log(description);
    //         const image = details.conference.location.picture_url;
    //         const imageTag = document.querySelector('.card-img-top');
    //         imageTag.innerHTML = image;
    //         imageTag.src = details.conference.location.picture_url;
    //         console.log(details);
    //     }
    //   }
    // } catch (e) {
    //   // Figure out what to do if an error is raised
    // //   console.error("error", error)
    // }




    const url = 'http://localhost:8000/api/conferences/';

    try {
      const response = await fetch(url);
  
      if (!response.ok) {
        // Figure out what to do when the response is bad


      } else {
        const data = await response.json();
  
        for (let conference of data.conferences) {
          const detailUrl = `http://localhost:8000${conference.href}`;
          const detailResponse = await fetch(detailUrl);
          if (detailResponse.ok) {
            const details = await detailResponse.json();

            // const { name, description, location: { picture_url: pictureUrl }, startDate, endDate } = details.conference;
            // const newStartDate = formatDate(startDate)
            // const newEndDate = formatDate(endDate);

            const name = details.conference.name;
            const description = details.conference.description;
            const pictureUrl = details.conference.location.picture_url;

            const startDate = details.conference.starts;
            let d1 = new Date(startDate);
            let newStartDate = (d1.getMonth()+1) + "/" + d1.getDate() + "/" + d1.getFullYear()
            const endDate = details.conference.ends;
            let d2 = new Date(endDate);
            let newEndDate = (d2.getMonth()+1) + "/" + d2.getDate() + "/" + d2.getFullYear()
            //  create the card
            const html = createCard(name, description, pictureUrl, newStartDate, newEndDate);

            const column = document.querySelector('.row');
            // add the card to the column
            column.innerHTML += html;
          }
        }
  
      }
    } catch (e) {
        console.error("error", e);
      // Figure out what to do if an error is raised
    }



});








