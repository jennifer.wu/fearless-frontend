import React from 'react';

class ConferenceForm extends React.Component {
    constructor (props) {
        super(props)
        this.state = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            locations: [],
        }


        this.handleNameChange = this.handleNameChange.bind(this);
        this.handleStartDateChange = this.handleStartDateChange.bind(this);
        this.handleEndDateChange = this.handleEndDateChange.bind(this);
        this.handleDescriptionChange = this.handleDescriptionChange.bind(this);
        this.handleMaxPresentationsChange = this. handleMaxPresentationsChange.bind(this);
        this.handleMaxAttendeesChange = this.handleMaxAttendeesChange.bind(this);
        this.handleLocationChange = this.handleLocationChange.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }

    handleNameChange(event) {
        const value = event.target.value;
        this.setState({name: value});
    }

    handleStartDateChange(event) {
        const value = event.target.value;
        this.setState({starts: value});
    }

    handleEndDateChange(event) {
        const value = event.target.value;
        this.setState({ends: value});
    }

    handleDescriptionChange(event) {
        const value = event.target.value;
        this.setState({description: value});
    }

    handleMaxPresentationsChange(event) {
        const value = event.target.value;
        this.setState({maxPresentations: value});
    }

    handleMaxAttendeesChange(event) {
        const value = event.target.value;
        this.setState({maxAttendees: value});
    }

    handleLocationChange(event) {
        const value = event.target.value;
        this.setState({location: value});
    }

    async handleSubmit(event) {
        event.preventDefault();
        const data = {...this.state};
        data.max_presentations = data.maxPresentations;
        data.max_attendees = data.maxAttendees;
        delete data.maxPresentations;
        delete data.maxAttendees;
        delete data.locations;
        console.log("DATA:", data);

        const locationUrl = 'http://localhost:8000/api/conferences/';
        const fetchConfig = {
          method: "post",
          body: JSON.stringify(data),
          headers: {
            'Content-Type': 'application/json',
          },
        };

        const response = await fetch(locationUrl, fetchConfig);
        if (response.ok) {
          const newLocation = await response.json();
          console.log(newLocation);

          const cleared = {
            name: "",
            starts: "",
            ends: "",
            description: "",
            maxPresentations: "",
            maxAttendees: "",
            location: "",
          };
          this.setState(cleared);
        }
    }
 

    async componentDidMount() {
        const url = 'http://localhost:8000/api/locations/';
    
        const response = await fetch(url);
        console.log("RESPONSE:", response)
    
        if (response.ok) {
          const data = await response.json();
          this.setState({locations: data.locations});
          console.log("locations:", data.locations);

        }
    }

    render () {
        return (
            <React.Fragment>
                  <div className="my-5 container">
                <form onSubmit ={this.handleSubmit} id="create-conference-form">
              <div className="form-floating mb-3">
                <input onChange={this.handleNameChange} placeholder="Name" required type="text" name="name" id="name" className="form-control" value={this.state.name} />
                <label htmlFor="name">Name</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleStartDateChange} placeholder="Start date" required type="date" name="starts" id="starts" className="form-control" value={this.state.starts}/>
                <label htmlFor="starts">Start date</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleEndDateChange} placeholder="End date" required type="date" name="ends" id="ends" className="form-control" value={this.state.ends} />
                <label htmlFor="ends">End date</label>
              </div>
              <div className="mb-3">
                  <label htmlFor="description">Description</label>
                  <textarea onChange={this.handleDescriptionChange} className = "form-control" value={this.state.description} name="description" id="description" rows="5"></textarea>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxPresentationsChange} placeholder="Max presentations" required type="number" name="max_presentations" id="max_presentations" className="form-control" value={this.state.maxPresentations} />
                <label htmlFor="max_presentations">Max presentations</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={this.handleMaxAttendeesChange} placeholder="Max attendees" required type="number" name="max_attendees" id="max_attendees" className="form-control" value={this.state.maxAttendees} />
                <label htmlFor="max_attendeess">Max attendees</label>
              </div>
              <div className="mb-3">
                <select onChange={this.handleLocationChange} required id="location" name="location" className="form-select" value={this.state.location} >
                <option value="">Select a location</option>
                {this.state.locations.map(location => {
                    return (
                        <option key={location.id} value={location.id}>
                            {location.name}
                        </option>
                    );
                })}
                </select>
            </div>
            <button className="btn btn-primary">Create</button>
            </form>
            </div>
            </React.Fragment>
        )
    }
};

export default ConferenceForm;