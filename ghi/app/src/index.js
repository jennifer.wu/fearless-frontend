import React from 'react';
import ReactDOM from 'react-dom/client';
import './index.css';
import App from './App';
import reportWebVitals from './reportWebVitals';

/* "root" and "root.render tells React where to take that "fake" HTML 
and put it */
const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <React.StrictMode>
    {/* runs the App function from the other file App.js */}
    <App />
  </React.StrictMode>
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();


// CAN'T DO THIS:
// const response = await fetch('http://localhost:8001/api/attendees/');
// console.log(response);


async function loadAttendees() {
  const response = await fetch('http://localhost:8001/api/attendees/');
  // console.log("RESPONSE:", response);

  if (response.ok) {
    const data = await response.json();
    // console.log("DATA:", data);
    
    root.render(
      <React.StrictMode>
        <App attendees={data.attendees} />
      </React.StrictMode>
    );

  } else {
    console.error("ERROR:", response);
  }

}
loadAttendees();