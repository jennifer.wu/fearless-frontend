import React from "react";

import {BrowserRouter, Routes, Route} from "react-router-dom";
import AttendeesList from "./AttendeesList";
import LocationForm from "./LocationForm";
import ConferenceForm from "./ConferenceForm";
import AttendConferenceForm from "./AttendConferenceForm";
import PresentationForm from "./PresentationForm";
import Nav from "./Nav.js";
import MainPage from "./MainPage";



function App(props) {
  if (props.attendees === undefined) {
    return null;
  }
  // if (props.locations === undefined) {
  //   return null;
  // }
  return (


        // <Routes>
        // <Route path="/attendees/new" element= {<AttendConferenceForm />} />
        // <Route path="/conferences/new" element= {<ConferenceForm />} />
        // <Route path="/locations/new	" element={<LocationForm />} />
        // <Route path="/attendees/new" element={<AttendeesList />} />
        // </Routes>


          <BrowserRouter>
      <Nav />
      <Routes>
        <Route index element={<MainPage />} />
        <Route path="presentations">
          <Route path="new" element={<PresentationForm />} />
        </Route>
        <Route path="attendees">
          <Route path="new" element={<AttendConferenceForm />} />
        </Route>
        <Route path="conferences">
          <Route path="new" element={<ConferenceForm />} />
        </Route>
        <Route path="locations">
          <Route path="new" element={<LocationForm />} />
        </Route>
        <Route path="attendees">
          <Route path="" element={<AttendeesList attendees={props.attendees} />} />
        </Route>
      </Routes>
    </BrowserRouter>
    









    // <React.Fragment>
    // <Nav />
    // <div className="container">
    // <AttendConferenceForm />
    // {/* <ConferenceForm /> */}
    // {/* <LocationForm /> */}
    // {/* <AttendeesList attendees={props.attendees} /> */}
    // </div>
    // </React.Fragment>
  );
}

export default App;






// {/* <table className="table table-striped">
// <thead>
//   <tr>
//     <th>Name</th>
//     <th>Conference</th>
//   </tr>
// </thead>
// <tbody>
//   {/* CAN'T DO THIS:
//   for (let attendee of props.attendees) {
//     <tr>
//       <td>{ attendee.name }</td>
//       <td>{ attendee.conference }</td>
//     </tr>
//   } */}
//   {props.attendees.map(attendee => {
//     return (
//       <tr key={attendee.href}>
//         <td>{ attendee.name }</td>
//         <td>{ attendee.conference }</td>
//       </tr>
//     );
//   })}
// </tbody>
// </table> */}